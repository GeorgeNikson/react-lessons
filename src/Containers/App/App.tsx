import React from "react";
import { Route, Switch } from "react-router-dom";
import AuthPageContainer from "../AuthPageContainer";
import MainPageContainer from "../MainPageContainer";
import RegisterPageContainer from "../RegisterPageContainer";

function App() {
  return (
    <>
      <Switch>
        <Route path="/" exact component={MainPageContainer} />
        <Route path="/auth" exact component={AuthPageContainer} />
        <Route path="/reg" exact component={RegisterPageContainer} />
      </Switch>
    </>
  );
}
export default App;
