import React, { FC, useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import AuthPage from "../../Components/AuthPage/AuthPage";
import { getUserData } from "../../store/Auth/selector";
import { getUserDataReg } from "../../store/Reg/selector";

const AuthPageContainer: FC = () => {
  const userData = useSelector(getUserData);
  const history = useHistory();
  const userDataReg = useSelector(getUserDataReg);

  useEffect(() => {
    if (userData.data.login && userData.data.password) {
      if (
        userData.data.login === userDataReg.data.login &&
        userData.data.password === userDataReg.data.password
      )
        history.push("/");
    }
  }, [history, userData, userDataReg]);

  return <AuthPage />;
};

export default AuthPageContainer;
