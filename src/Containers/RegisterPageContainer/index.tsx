import React, { FC, useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import RegisterPage from "../../Components/RegisterPage";
import { getUserDataReg } from "../../store/Reg/selector";

const RegisterPageContainer: FC = () => {
  const history = useHistory();
  const userDataReg = useSelector(getUserDataReg);

  useEffect(() => {
    if (
      userDataReg.data &&
      userDataReg.data.email &&
      userDataReg.data.password &&
      userDataReg.data.passRepeated
    ) {
      history.push("/auth");
    }
  }, [userDataReg, history]);

  return <RegisterPage />;
};

export default RegisterPageContainer;
