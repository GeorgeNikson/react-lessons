import React from "react";
import MainPage from "../../Components/MainPage/MainPage";

class MainPageContainer extends React.PureComponent {
  render() {
    return <MainPage />;
  }
}

export default MainPageContainer;
