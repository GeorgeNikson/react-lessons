import { combineReducers } from "redux";
import authReducer from "./Auth/authReducer";
import regReducer from "./Reg/regReducer";

const rootReducer = combineReducers({
  auth: authReducer,
  reg: regReducer,
});

export default rootReducer;
