export enum Actions {
  setUserData = "SET_USER_DATA",
}

export const setUserDataAction = (data: any) => {
  return {
    type: Actions.setUserData,
    payload: { ...data },
  };
};
