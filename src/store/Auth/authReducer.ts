import { Actions } from "./actions";
import { TReducer } from "./types";

const initialState: TReducer = {
  data: {
    login: "",
    password: "",
  },
};

const authReducer = (
  state = initialState,
  action: {
    type: string;
    payload?: any;
  }
) => {
  switch (action.type) {
    case Actions.setUserData:
      return {
        ...state,
        data: action.payload,
      };

    default:
      return state;
  }
};

export default authReducer;
