import { TReducerRoot } from "../types";

// eslint-disable-next-line import/prefer-default-export
export const getUserData = (state: TReducerRoot) => state.auth;
