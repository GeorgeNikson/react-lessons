import { Actions } from "./actions";
import { TReducerReg } from "./types";

const initialState: TReducerReg = {
  data: {
    login: "",
    email: "",
    password: "",
    passRepeated: "",
  },
};

const regReducer = (
  state = initialState,
  action: {
    type: string;
    payload?: any;
  }
) => {
  switch (action.type) {
    case Actions.setUserDataReg:
      return {
        ...state,
        data: action.payload,
      };
    default:
      return state;
  }
};

export default regReducer;
