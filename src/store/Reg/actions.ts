export enum Actions {
  setUserDataReg = "SET_USER_DATA_REG",
}

export const setUserDataRegAction = (data: any) => {
  return {
    type: Actions.setUserDataReg,
    payload: { ...data },
  };
};
