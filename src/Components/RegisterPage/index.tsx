import React, { FC } from "react";
import PageWrapper from "../Common/Wrappers/PageWrapper";
// import style from "./RegisterPage.module.scss";
import RegistrationForm from "./RegistrationForm";

const RegisterPage: FC = () => {
  return (
    <PageWrapper>
      <RegistrationForm />
    </PageWrapper>
  );
};

export default RegisterPage;
