const validateForm = (valuesFromFields: any) => {
  // eslint-disable-next-line prefer-const
  let errors: any = {};
  let bool = true;
  if (!valuesFromFields.login.trim()) {
    errors.login = "Login required!";
  }
  if (!valuesFromFields.email) {
    errors.email = "Email required!";
  }
  if (valuesFromFields.password.length < 6) {
    errors.password = "Password should be 6 signs or more";
  }
  if (valuesFromFields.passRepeated !== valuesFromFields.password) {
    errors.passRepeated = "Passwords should match";
  }
  Object.values(errors).map((el: any) => {
    if (el) {
      bool = false;
      errors.bool = bool;
    }
    return errors.bool;
  });
  return errors;
};

export default validateForm;
