import React, { FC, useState } from "react";
import { useDispatch } from "react-redux";
import { setUserDataRegAction } from "../../../store/Reg/actions";
import PasswordInput from "../../Common/Form/PasswordInput";
import TextInput from "../../Common/Form/TextInput";
import style from "./RegistrationForm.module.scss";
import validateForm from "./validation";

const RegistrationForm: FC = () => {
  const [login, setLogin] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [passRepeated, setPassRepeated] = useState<string>("");
  const [validationError, setValidationError] = useState<any>({});
  const [isError, setIsError] = useState<boolean>(true);

  const dispatch = useDispatch();

  const onSubmit = () => {
    setValidationError(validateForm({ login, email, password, passRepeated }));
    setIsError(validateForm({ login, email, password, passRepeated }).bool);
    if (password === passRepeated) {
      dispatch(setUserDataRegAction({ login, email, password, passRepeated }));
    }
  };

  const enterPressed = (e: any) => {
    if (e.key === "Enter") {
      onSubmit();
    }
  };

  return (
    <div className={style.register_form_wrapper}>
      <h3>Please create new account</h3>
      <TextInput
        placeholder="Login"
        handler={setLogin}
        onKeyHandler={enterPressed}
        error={validationError.login}
        errorStyle={style.register_form_err}
        isError={isError}
      />
      <TextInput
        placeholder="Email"
        handler={setEmail}
        onKeyHandler={enterPressed}
        error={validationError.email}
        errorStyle={style.register_form_err}
        isError={isError}
      />
      <PasswordInput
        placeholder="Password"
        handler={setPassword}
        onKeyHandler={enterPressed}
        error={validationError.password}
        errorStyle={style.register_form_err}
        isError={isError}
      />
      <PasswordInput
        placeholder="Repeat password"
        handler={setPassRepeated}
        onKeyHandler={enterPressed}
        error={validationError.passRepeated}
        errorStyle={style.register_form_err}
        isError={isError}
      />
      <button
        type="button"
        className={style.register_form_btn}
        onClick={onSubmit}>
        Create
      </button>
    </div>
  );
};

export default RegistrationForm;
