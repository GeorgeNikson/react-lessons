import React, { FC, useState } from "react";
import PageWrapper from "../Common/Wrappers/PageWrapper";
import ShopCard from "../ShopCard/ShopCard";
import style from "./MainPage.module.scss";
import IPhoneImg from "../../Assets/image/IPhone.png";
import Macbook from "../../Assets/image/Macbook.png";

const MainPage: FC = () => {
  const [counter, setCounter] = useState<number>(0);

  const handler = () => {
    if (counter < 2) setCounter(counter + 1);
  };

  return (
    <>
      <PageWrapper>
        <ShopCard title="IPhone" limit={12} srcForImg={IPhoneImg} />
        <ShopCard title="MacBook" limit={16} srcForImg={Macbook} />
        {counter === 1 && <ShopCard title="Nintendo Switch" limit={15} />}
        {counter > 1 && (
          <>
            <ShopCard title="Nintendo Switch" limit={15} />
            <ShopCard title="PlayStation" limit={5} />
          </>
        )}
        <div className={style.addCardBtn}>
          {counter < 2 && (
            <button type="button" onClick={handler}>
              Show More
            </button>
          )}
        </div>
      </PageWrapper>
    </>
  );
};

export default MainPage;
