import React, { FC, useState } from "react";
import { useDispatch } from "react-redux";
import { setUserDataAction } from "../../../store/Auth/actions";
import PasswordInput from "../../Common/Form/PasswordInput";
import TextInput from "../../Common/Form/TextInput";
import style from "./AuthForm.module.scss";

const AuthFrom: FC = () => {
  const [login, setLogin] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const dispatch = useDispatch();

  const onSubmit = () => {
    dispatch(setUserDataAction({ login, password }));
  };

  const enterPressed = (e: any) => {
    if (e.key === "Enter") {
      onSubmit();
    }
  };

  return (
    <div className={style.auth_form_wrapper}>
      <h3>Auth Form</h3>
      <TextInput
        placeholder="Login"
        handler={setLogin}
        onKeyHandler={enterPressed}
      />
      <PasswordInput
        placeholder="Password"
        handler={setPassword}
        onKeyHandler={enterPressed}
      />
      <button type="button" className={style.auth_form_btn} onClick={onSubmit}>
        Login
      </button>
    </div>
  );
};

export default AuthFrom;
