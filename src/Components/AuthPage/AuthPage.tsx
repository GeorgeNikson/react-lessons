import React, { FC } from "react";
import PageWrapper from "../Common/Wrappers/PageWrapper";
import AuthFrom from "./AuthForm/AuthForm";
// import style from "./AuthPage.module.scss";

const AuthPage: FC = () => {
  return (
    <PageWrapper>
      <AuthFrom />
    </PageWrapper>
  );
};

export default AuthPage;
