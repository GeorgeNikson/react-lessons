import React from "react";
import style from "./Footer.module.scss";

const Footer: React.FC = () => {
  return (
    <>
      <footer className={style.footer_wrap}>
        <div className={style.footer_lorem}>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima
          laboriosam minus sunt, nemo ipsum hic voluptas fugiat doloribus,
          similique est aliquam sapiente excepturi debitis ab deleniti inventore
          architecto corporis labore!
        </div>
        <div className={style.footer_year}>&copy; 2021г.</div>
      </footer>
    </>
  );
};

export default Footer;
