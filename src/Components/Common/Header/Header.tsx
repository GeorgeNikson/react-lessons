import React from "react";
import Authorization from "./Authorization/Authorization";
import style from "./Header.module.scss";
import Logo from "./Logo/Logo";
import NavBar from "./NavBar/NavBar";

const Header: React.FC = () => {
  return (
    <>
      <header className={style.header_wrap}>
        <Logo />
        <NavBar />
        <Authorization />
      </header>
    </>
  );
};

export default Header;
