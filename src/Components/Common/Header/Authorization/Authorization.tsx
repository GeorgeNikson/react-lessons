import React, { FC } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { getUserData } from "../../../../store/Auth/selector";
import { getUserDataReg } from "../../../../store/Reg/selector";
import style from "./Authorization.module.scss";

const Authorization: FC = () => {
  const userData = useSelector(getUserData);
  const userDataReg = useSelector(getUserDataReg);
  let isData = false;

  if (
    userData.data.login &&
    userData.data.password &&
    userData.data.login === userDataReg.data.login &&
    userData.data.password === userDataReg.data.password
  ) {
    isData = true;
  }

  return (
    <>
      <div className={style.authorization_wrap}>
        <ul>
          {!isData && (
            <>
              <li>
                <Link to="/auth" className={style.link}>
                  Авторизация
                </Link>
              </li>
              <li>
                <Link to="/reg" className={style.link}>
                  Регистрация
                </Link>
              </li>
            </>
          )}
          {isData && <li>{userData.data.login}</li>}
        </ul>
      </div>
    </>
  );
};

export default Authorization;
