import React from "react";
import { Link } from "react-router-dom";
import style from "./NavBar.module.scss";

const NavBar: React.FC = () => {
  return (
    <>
      <nav className={style.nav_wrap}>
        <ul className={style.nav_li}>
          <li>
            <Link to="/" className={style.link}>
              Главная
            </Link>
          </li>
          <li>Какой-то раздел</li>
          <li>О нас</li>
        </ul>
      </nav>
    </>
  );
};

export default NavBar;
