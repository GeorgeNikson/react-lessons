import React from "react";
import style from "./Logo.module.scss";
import logo from "../../../../Assets/image/logo.jpg";

const Logo: React.FC = () => {
  return (
    <>
      <div className={style.logo_wrap}>
        <img src={logo} alt="logo" />
      </div>
    </>
  );
};

export default Logo;
