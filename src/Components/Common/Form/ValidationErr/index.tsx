import React, { FC } from "react";

type TProps = {
  error: string;
  style: any;
};

const ValidationErr: FC<TProps> = (props: TProps) => {
  const { error, style } = props;
  return <div className={style}>{error}</div>;
};

export default ValidationErr;
