import React, { FC } from "react";
import style from "../Form.module.scss";
import ValidationErr from "../ValidationErr";

type TProps = {
  placeholder: string;
  handler: React.Dispatch<React.SetStateAction<string>>;
  onKeyHandler?: (e: any) => void;
  error?: any;
  errorStyle?: any;
  isError?: boolean;
};

const TextInput: FC<TProps> = (props: TProps) => {
  const {
    placeholder,
    handler,
    onKeyHandler,
    error,
    errorStyle,
    isError,
  } = props;

  const textInputHanler = (e: any) => {
    handler(e.currentTarget.value);
  };

  return (
    <>
      <input
        type="text"
        className={style.auth_form_input}
        onChange={textInputHanler}
        onKeyPress={onKeyHandler}
        placeholder={placeholder}
      />
      {!isError && <ValidationErr error={error} style={errorStyle} />}
    </>
  );
};

export default TextInput;
