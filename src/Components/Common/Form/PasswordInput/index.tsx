import React, { FC } from "react";
import style from "../Form.module.scss";
import ValidationErr from "../ValidationErr";

type TProps = {
  placeholder: string;
  handler: React.Dispatch<React.SetStateAction<string>>;
  onKeyHandler: (e: any) => void;
  error?: any;
  errorStyle?: any;
  isError?: boolean;
};

const PasswordInput: FC<TProps> = (props: TProps) => {
  const {
    placeholder,
    handler,
    onKeyHandler,
    error,
    errorStyle,
    isError,
  } = props;

  return (
    <>
      <input
        type="password"
        className={style.auth_form_input}
        name="password"
        onChange={(e) => handler(e.currentTarget.value)}
        placeholder={placeholder}
        onKeyPress={onKeyHandler}
      />
      {!isError && <ValidationErr error={error} style={errorStyle} />}
    </>
  );
};

export default PasswordInput;
