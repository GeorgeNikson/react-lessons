import React from "react";
import Button from "./Button/Button";
import Card from "./Card/Card";
import style from "./ShopCard.module.scss";

interface IProps {
  title: string;
  limit: number;
  srcForImg?: any;
}

interface IState {
  count: number;
  increment: number;
  decrement: number;
}

class ShopCard extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = { count: 0, increment: 1, decrement: 1 };
  }

  incrementHandler = (increment: number) => () => {
    const { count } = this.state;
    this.setState({ count: count + increment });
  };

  decrementHandler = (decrement: number) => () => {
    const { count } = this.state;
    this.setState({ count: count - decrement });
  };

  render() {
    const { title, limit, srcForImg } = this.props;
    const { count, increment, decrement } = this.state;
    return (
      <div className={style.shop_card_wrap}>
        <Card title={title} count={count} srcForImg={srcForImg}>
          <Button
            title={`Удалить ${decrement} ${title}`}
            type="decrement"
            handler={this.decrementHandler(decrement)}
            limit={limit}
            count={count}
          />
          <Button
            title={`Добавить ${increment} ${title}`}
            type="increment"
            handler={this.incrementHandler(increment)}
            limit={limit}
            count={count}
          />
        </Card>
      </div>
    );
  }
}

export default ShopCard;
