import React from "react";
import style from "./Card.module.scss";

interface IProps {
  title: string;
  count: number;
  srcForImg?: any;
}

class Card extends React.PureComponent<IProps> {
  render() {
    const { title, count, srcForImg, children } = this.props;

    return (
      <>
        <div className={style.card_wrap}>
          <img src={srcForImg} alt="" />
          <h1>
            {title} <br /> {count} шт.
          </h1>
          <div className={style.card_style}>{children}</div>
        </div>
      </>
    );
  }
}

export default Card;
