import React from "react";
import style from "./Button.module.scss";

interface IProps {
  title: string;
  count: number;
  type: string;
  handler(): void;
  limit: number;
}

class Button extends React.PureComponent<IProps> {
  render() {
    const { title, count, type, handler, limit } = this.props;
    return (
      <>
        <button
          className={`${style.button} 
            ${
              type === "increment"
                ? style.button_increment
                : style.button_decrement
            }`}
          type="button"
          onClick={handler}
          disabled={
            (type === "increment" && count === limit) ||
            (type === "decrement" && count === 0)
          }>
          {(type === "increment" &&
            count === limit &&
            "Максимальное количество") ||
            (type === "decrement" && count === 0 && "Удалить") ||
            title}
        </button>
      </>
    );
  }
}

export default Button;
